import org.scalatest.GivenWhenThen
import org.scalatest.funspec.AnyFunSpec
import ReversePolishNotation.Calculator

class FormulaSpec extends AnyFunSpec with GivenWhenThen {

  describe("an empty formula") {
    it("should evaluate to zero") {
      assert("".calc == 0)
    }
  }

  describe("a value") {
    it("should evaluate to the number it stands for") {
      assert("5".calc == 5)
    }
  }

  describe("a sum of two values") {
    it("should be applied to the evaluations of the values") {
      assert("1 2 +".calc == 3)
    }
  }

  describe("a product of two values") {
    it("should be applied to the evaluations of the values") {
      assert("1 2 *".calc == 2)
    }
  }

  describe("a subtraction between two values") {
    it("should be applied to the evaluations of the values") {
      assert("3 7 -".calc == "-4".calc)
    }
  }

  describe("a division between two values") {
    it("should be applied to the evaluations of the values") {
      assert("7 3 /".calc == "2".calc)
    }
  }

  describe("a binary operation on a value and a subformula") {
    it("should apply to the evaluations of the value and the subformula") {
      assert("2 3 * 1 +".calc == "2 3 *".calc + "1".calc)
    }
  }

  describe("a binary operation on a subformula and a value") {
    it("should apply to the evaluations of the subformula and the value") {
      assert("1 2 3 * +".calc == "1".calc + "2 3 *".calc)
    }
  }

  describe("a binary operation on two subformulas") {
    it("should apply to the evaluations of the two subsformulas") {
      assert("2 3 + 0 1 + +".calc == "2 3 +".calc + "0 1 +".calc)
    }
  }
}
