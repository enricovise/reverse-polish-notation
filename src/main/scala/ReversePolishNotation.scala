object ReversePolishNotation {

  implicit class Calculator(input: String) {

    def calc: Double = {
      input.split(" ").foldLeft(List[Int]()) {
        case (x :: y :: rest, "+") => y + x :: rest
        case (x :: y :: rest, "*") => y * x :: rest
        case (x :: y :: rest, "-") => y - x :: rest
        case (x :: y :: rest, "/") => y / x :: rest
        case (list, "") => 0 :: list
        case (list, num) => num.toInt :: list
      }.head
    }
  }
}
